We study the impact of Production Credit Associations (PCAs) during the decade-long period shortly after their introduction as one component of the 1916 Federal Farm Loan Act. 
Using county distances to PCAs as a proxy for cost of access to credit, we examine the effects of credit expansion on county-level crop yield, crop revenue, and input use. 
Despite serving only about 7\% of U.S. farmers during the period we study, we estimate that counties 100 kilometers closer to a PCA had roughly 10\% higher crop revenue per acre. 
We also find that counties closer to PCA locations experienced significantly higher growth rates in tractor and fertilizer utilization, relative to more distant counties. 
In years prior to the arrival of PCAs, farms in relatively close-by counties earn on average less revenue and use fewer purchased inputs than farms in counties further away. 
This relationship \emph{reverses} in subsequent years, suggesting that the mechanism for identifying PCA locations (one per state, initially) targeted less well-off counties. 
Our estimates therefore represent lower bounds for the true causal effect of access to credit on farm revenue and input use during the period we study.
